const bcrypt = require('bcrypt')
const SALT_ROUNDS = 10


const jwt = require('jsonwebtoken')
const dotenv = require('dotenv')
dotenv.config()
const TOKEN_SECRET = process.env.TOKEN_SECRET

const mysql = require('mysql')

const connection = mysql.createConnection({
    host : 'localhost',
    user : 'admin',
    password : 'admin',
    database: 'BookHotel'
    
});


connection.connect();
const express = require('express')
const app = express()
const port = 4000

function authenticateToken(req,res,next) {
    const authHeader = req.headers['authorization']
    const token = authHeader && authHeader.split(' ')[1]
    if (token==null) return res.sendStatus(401)
    jwt.verify(token,TOKEN_SECRET,(err,user) => {
        if(err) { return res.sendStatus(403) }
        else{
            req.user=user
            next()
        }
    })
}


app.get("/list_booking", (req,res) =>{
    query = `SELECT Booker.BookerID, Booker.BookerName, Booker.BookerSurname,
                    Room.RoomName, Booking.BookTime 
             FROM Booker,Booking,Room
            WHERE (Booking.BookerID = Booker.BookerID) AND 
            (Booking.RoomID = Room.RoomID);`;


    connection.query( query,(err,rows) => {
        if (err) {
            res.json({
                "status" : "400",
                "message" : "Error querying from running db"
            
            })
        }else {
            res.json(rows);
        }
    });

})

app.post("/register_booker",authenticateToken, (req, res) =>{
    
    let booker_name = req.query.booker_name
    let booker_surname = req.query.booker_surname
    let booker_username = req.query.booker_username
    let booker_password = req.query.booker_password


    bcrypt.hash(booker_password, SALT_ROUNDS, (err,hash)=> {

            let query =  `INSERT INTO Booker
                                    (BookerName, BookerSurname, Username, Password) 
                                    VALUES ('${booker_name}', '${booker_surname}',
                                            '${booker_username}' , '${hash}')`

                console.log(query)

                connection.query( query, (err, rows) => {
                    if (err){
                        res.json({
                                    "status" : "400", 
                                    "message" : "Error inserting data into db"
                                })
                    }else{
                        res.json({
                            "status" : "200", 
                            "message" : "Adding new booker successful"
                        })
                    }
                });


    })
    

});





app.post("/login", (req, res) =>{
    let username = req.query.username
    let user_password = req.query.password
    let query =`SELECT * FROM Booker WHERE Username='${username}'`
   
    connection.query( query, (err, rows) => {
            if (err){
                console.log(err)
                res.json({
                            "status" : "400", 
                            "message" : "Error querying from booking db"
                        })
            }else{

                let db_password = rows[0].Password
                bcrypt.compare(user_password, db_password, (err,result)=> {
                    if (result) {
                        let payload = {
                            "username" : rows[0].Username,
                            "user_id" : rows[0].BookerID,
                            "IsAdmin" : rows[0].IsAdmin
                        }
                        console.log(payload)
                        let token = jwt.sign(payload, TOKEN_SECRET,{expiresIn:'1d'} )
                        res.send(token)
                    }else {res.send("Invalid username / password") }

                })
                
            }
        })


})        


app.post("/add_room", (req, res) =>{
    let room_name = req.query.room_name
    let room_price = req.query.room_price

    let query =  `INSERT INTO Room 
                        (RoomName, RoomPrice) 
                        VALUES ('${room_name}', '${room_price}')`
    console.log(query)

    connection.query( query, (err, rows) => {
        if (err){
            res.json({
                        "status" : "400", 
                        "message" : "Error inserting data into db"
                    })
        }else{
            res.json({
                "status" : "200", 
                "message" : "Adding room successful"
            })
        }
        });


})


app.get("/list_booker",authenticateToken, (req,res) =>{
    query = "SELECT * from Booker";
    connection.query( query,(err,rows) => {
        if (err) {
            res.json({
                "status" : "400",
                "message" : "Error querying from booker db"
            
            })
        }else {
            res.json(rows);
        }
    });

})





app.post("/update_booker", (req, res) =>{
    let booker_id = req.query.booker_id
    let booker_name = req.query.booker_name
    let booker_surname = req.query.booker_surname

    let query =  `UPDATE Booker SET
                    BookerName='${booker_name}',
                    BookerSurname='${booker_surname}'
                    WHERE BookerID='${booker_id}'`
                    
    console.log(query)

    connection.query( query, (err, rows) => {
        if (err){
            res.json({
                        "status" : "400", 
                        "message" : "Error updating record"
                    })
        }else{
            res.json({
                "status" : "200", 
                "message" : "Updating booker successful"
            })
        }
        });


})

app.post("/update_room", (req, res) =>{
    let room_id = req.query.room_id
    let room_name = req.query.room_name
    let room_price = req.query.room_price

    let query =  `UPDATE Room SET
                    RoomName='${room_name}',
                    RoomPrice='${room_price}'
                    WHERE RoomID=${room_id}`
                    
    console.log(query)

    connection.query( query, (err, rows) => {
        if (err){
            res.json({
                        "status" : "400", 
                        "message" : "Error updating record"
                    })
        }else{
            res.json({
                "status" : "200", 
                "message" : "Updating room successful"
            })
        }
        });
    })



    app.post("/delete_booker", (req, res) =>{
        let booker_id = req.query.booker_id
        
    
        let query =  `DELETE FROM Booker WHERE bookerID=${booker_id}`
                        
        console.log(query)
    
        connection.query( query, (err, rows) => {
            if (err){
                res.json({
                            "status" : "400", 
                            "message" : "Error deleting record"
                        })
            }else{
                res.json({
                    "status" : "200", 
                    "message" : "Deleting record successful"
                })
            }
            });
    
    
    })


    app.post("/delete_room", (req, res) =>{
        let room_id = req.query.room_id
        
    
        let query =  `DELETE FROM Room WHERE RoomID=${room_id}`
                        
        console.log(query)
    
        connection.query( query, (err, rows) => {
            if (err){
                res.json({
                            "status" : "400", 
                            "message" : "Error deleting record"
                        })
            }else{
                res.json({
                    "status" : "200", 
                    "message" : "Deleting record successful"
                })
            }
            });
    
    
    })

app.listen(port, () => {
    console.log("Now starting Booking Hotel Backend at port "+port)

})
